package com.john.lewis.ascii.avatar.service;

import com.john.lewis.ascii.avatar.InvalidInputException.InvalidInputException;
import com.john.lewis.ascii.avatar.service.impl.AsciiAvatarServiceImpl;
import com.john.lewis.ascii.avatar.service.impl.ImageToAsciiImpl;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.*;

/**
 * Created by Praveen.R on 05/10/2016.
 */
public class AsciiAvatarServiceTest {

    private ImageToAscii imageToAscii;

    private AsciiAvatarService asciiAvatarService;

    @Before
    public void setUp(){
        imageToAscii = new ImageToAsciiImpl(150,100);
        asciiAvatarService = new AsciiAvatarServiceImpl(imageToAscii);
    }

    @Test(expected = InvalidInputException.class)
    public void testInvalidInputException() throws Exception {
        asciiAvatarService.validateAndConvert(new String[0]);
    }

    @Test(expected = IOException.class)
    public void testIOException()throws Exception{
        String[] input = {"invalid.png"};
        asciiAvatarService.validateAndConvert(input);
    }

    @Test
    public void testValidInput()throws Exception{
        URL url = this.getClass().getClassLoader().getResource("java8_logo.png");
        String[] input = {url.getFile()} ;
        String result = asciiAvatarService.validateAndConvert(input);

        URL urlDest = this.getClass().getClassLoader().getResource("java8_asciiart.txt");
        Path resPath = Paths.get(urlDest.toURI());
        String fileContent = new String(java.nio.file.Files.readAllBytes(resPath), "UTF8");
        fileContent += '\n';
        assertEquals(result, fileContent);
    }
}