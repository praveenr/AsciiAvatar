package com.john.lewis.ascii.avatar.service.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

/**
 * Created by Praveen.R on 05/10/2016.
 */
@Configuration
@ComponentScan("com.john.lewis.ascii.avatar.service")
@PropertySource("classpath:application.properties")
public class AsciiAvatarSpringBeansConfig {
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}