package com.john.lewis.ascii.avatar.service;

import com.john.lewis.ascii.avatar.service.config.AsciiAvatarSpringBeansConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

/**
 * Created by Praveen.R on 05/10/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AsciiAvatarSpringBeansConfig.class)
public class ImageToAsciiTest {
    @Autowired
    private ImageToAscii imageToAscii;

    @Test
    public void testConvertToASCII() throws Exception {
        URL url = this.getClass().getClassLoader().getResource("1068.png");
        String fileName = url.getFile();
        String result = imageToAscii.convertImageToASCII(fileName);

        URL urlDest = this.getClass().getClassLoader().getResource("1068_asciiart.txt");
        Path resPath = Paths.get(urlDest.toURI());
        String fileContent = new String(Files.readAllBytes(resPath), "UTF8");

        assertEquals(result, fileContent);
    }

    @Test
    public void testConvertToASCIIJavaLogo() throws Exception {
        URL url = this.getClass().getClassLoader().getResource("java8_logo.png");
        String fileName = url.getFile();
        String result = imageToAscii.convertImageToASCII(fileName);

        URL urlDest = this.getClass().getClassLoader().getResource("java8_asciiart.txt");
        Path resPath = Paths.get(urlDest.toURI());
        String fileContent = new String(Files.readAllBytes(resPath), "UTF8");

        assertEquals(result, fileContent);
    }

    @Test
    public void testConvertToASCIIBirdLogo() throws Exception {
        URL url = this.getClass().getClassLoader().getResource("Redwingblackbird1.jpg");
        String fileName = url.getFile();
        String result = imageToAscii.convertImageToASCII(fileName);

        URL urlDest = this.getClass().getClassLoader().getResource("Redwingblackbird1_asciiart.txt");
        Path resPath = Paths.get(urlDest.toURI());
        String fileContent = new String(Files.readAllBytes(resPath), "UTF8");

        assertEquals(result, fileContent);
    }

    @Test
    public void testConvertToASCIISanLogo() throws Exception {
        URL url = this.getClass().getClassLoader().getResource("sankranti.png");
        String fileName = url.getFile();
        String result = imageToAscii.convertImageToASCII(fileName);

        URL urlDest = this.getClass().getClassLoader().getResource("sankranti_asciiart.txt");
        Path resPath = Paths.get(urlDest.toURI());
        String fileContent = new String(Files.readAllBytes(resPath), "UTF8");

        assertEquals(result, fileContent);
    }
}