package com.john.lewis.ascii.avatar.application;

import com.john.lewis.ascii.avatar.InvalidInputException.InvalidInputException;
import com.john.lewis.ascii.avatar.config.AsciiAvatarSpringBeansConfig;
import com.john.lewis.ascii.avatar.service.impl.AsciiAvatarServiceImpl;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;

/**
 * Created by Praveen.R on 05/10/2016.
 */
public class AsciiAvatarApp {
    public static void main(String args[]) {
        try {
            final ApplicationContext applicationContext = new AnnotationConfigApplicationContext(AsciiAvatarSpringBeansConfig.class);
            AsciiAvatarServiceImpl asciiAvatarService = (AsciiAvatarServiceImpl) applicationContext.getBean("asciiAvatarService");
            System.out.println(asciiAvatarService.validateAndConvert(args));
        } catch (InvalidInputException e) {
            System.out.println("Invalid input provided" + e.getMessage());
        } catch (IOException e) {
            System.out.println("Exception while reading file :" + e.getMessage());
        }
    }
}
