package com.john.lewis.ascii.avatar.InvalidInputException;

/**
 * Created by Praveen.R on 05/10/2016.
 * Thrown when the input provided is invalid
 */
public class InvalidInputException extends Exception {
    public InvalidInputException(String message) {
        super(message);
    }
}
