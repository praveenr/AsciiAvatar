package com.john.lewis.ascii.avatar.service;

import com.john.lewis.ascii.avatar.InvalidInputException.InvalidInputException;

import java.io.IOException;

/**
 * Created by Praveen.R on 05/10/2016.
 * Service validates the input and sends the input to service to covert to Ascii output
 */
public interface AsciiAvatarService {
    /**
     * Validates input and generates ascii output for valid input.
     * @param input
     * @return ascii output.
     * @throws InvalidInputException
     * @throws IOException
     */
    String validateAndConvert(String input[]) throws InvalidInputException, IOException;
}
