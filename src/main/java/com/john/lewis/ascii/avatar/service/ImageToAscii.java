package com.john.lewis.ascii.avatar.service;

import java.io.IOException;

/**
 * Created by Praveen.R on 05/10/2016.
 * Takes the width and height from the properties file.
 * Resizes the image as per that width and height and generates ascii output.
 */
public interface ImageToAscii {

    /**
     * Resizes and converts the given image to Ascii text.
     * @param imgName
     * @return
     * @throws IOException
     */
    String convertImageToASCII(String imgName) throws IOException;
}
