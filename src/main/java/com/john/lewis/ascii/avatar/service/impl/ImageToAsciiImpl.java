package com.john.lewis.ascii.avatar.service.impl;

import com.john.lewis.ascii.avatar.service.ImageToAscii;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import org.imgscalr.Scalr;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Praveen.R on 05/10/2016.
 * Takes the width and height from the properties file.
 * Resizes the image as per that width and height and generates ascii output.
 */
@Service("imageToAsciiService")
public class ImageToAsciiImpl implements ImageToAscii {
    private int width;
    private int height;
    @Autowired
    public ImageToAsciiImpl(@Value("${image.width}")int width,
                            @Value("${image.height}")int height) {
        this.width = width;
        this.height = height;
    }

    /**
     * Resizes and converts the given image to Ascii text.
     * @param imgName
     * @return
     * @throws IOException
     */
    public String convertImageToASCII(String imgName) throws IOException {
        StringBuilder  imageInASCII = new StringBuilder();
        BufferedImage img = getReSizeBufferedImage(imgName);

        for (int i = 0; i < img.getHeight(); i=i+1) {
            for (int j = 0; j < img.getWidth(); j++) {
                Color pixcol = new Color(img.getRGB(j, i));
                imageInASCII.append(buildCharacter(pixcol));
            }
            imageInASCII.append( '\n' );
        }

        return imageInASCII.toString();
    }

    private BufferedImage getReSizeBufferedImage(String imgName) throws IOException{
        BufferedImage originalImg = ImageIO.read(new File(imgName));
        return Scalr.resize(originalImg, Scalr.Method.SPEED, Scalr.Mode.FIT_TO_HEIGHT,
                                width, height,Scalr.OP_ANTIALIAS);
    }

    private char buildCharacter( Color pixcol) {
        // overall pixel darkness
        double pixAvg = ((pixcol.getRed() * 0.30) + (pixcol.getBlue() * 0.59)
                                                  + (pixcol.getGreen() * 0.11));
        if (pixAvg >= 240) {
            return ' ';
        } else if (pixAvg >= 210) {
            return '.';
        } else if (pixAvg >= 190) {
            return ':';
        } else if (pixAvg >= 170) {
            return '-';
        } else if (pixAvg >= 120) {
            return '=';
        } else if (pixAvg >= 110) {
            return '+';
        } else if (pixAvg >= 80) {
            return '*';
        } else if (pixAvg >= 60) {
            return '#';
        } else if (pixAvg >= 40) {
            return '%';
        } else {
            return '@';
        }
    }
}
