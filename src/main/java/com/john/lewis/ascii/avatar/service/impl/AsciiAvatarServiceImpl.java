package com.john.lewis.ascii.avatar.service.impl;

import com.john.lewis.ascii.avatar.InvalidInputException.InvalidInputException;
import com.john.lewis.ascii.avatar.service.AsciiAvatarService;
import com.john.lewis.ascii.avatar.service.ImageToAscii;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * Created by Praveen.R on 05/10/2016.
 * Service validates the input and sends the input to service to covert to Ascii output
 */
@Service("asciiAvatarService")
public class AsciiAvatarServiceImpl implements AsciiAvatarService {

    private ImageToAscii imageToAscii;

    @Autowired
    public AsciiAvatarServiceImpl(ImageToAscii imageToAscii){
        this.imageToAscii = imageToAscii;
    }

    /**
     * Validates input and generates ascii output for valid input.
     * @param input
     * @return ascii output.
     * @throws InvalidInputException
     * @throws IOException
     */
    public String validateAndConvert(String input[]) throws InvalidInputException, IOException {
        if(input.length == 0) {
            throw new InvalidInputException("\nUsage : <Application Name> <FileName>");

        } else {
            StringBuilder output = new StringBuilder();
            for (String fileName : input) {
                output.append(imageToAscii.convertImageToASCII(fileName));
                output.append('\n');
            }
            return output.toString();
        }
    }
}
