Ascii Avatar
============

C.I : [![build status](https://gitlab.com/praveenr/AsciiAvatar/badges/master/build.svg)](https://gitlab.com/praveenr/AsciiAvatar/commits/master)

code coverage :[![codecov](https://codecov.io/gl/praveenr/AsciiAvatar/branch/master/graph/badge.svg)](https://codecov.io/gl/praveenr/AsciiAvatar)

basic requirements
------------------
Create a tool that can convert a users picture into ASCII artwork.

1. The game’s text window is 100 characters tall and 150 wide, so any ASCII art should fit into those dimensions

2. The aspect ratio of the user’s picture should be maintained

3. For now the characters " .:­=+*#%@" can be used to represent everything from white to black

4. The tool will work from a command line, allowing him to provide the name of a file and receiving the artwork as a standard output

Build
-----

Please import the project in eclipse or Intellij.

Use Maven command
```
    mvn clean
    mvn install
```

Usage
-----
```
AsciiAvatarApp <imageFileName>
```
Example
-------
```
AsciiAvatarApp imageFile
```